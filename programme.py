import math
import sys 
commit= str(sys.argv[1])
def calcul_factoriel(n):
     return(math.factorial(n))


def main():
    with open('data.txt') as file:
        res=[]
        
        for i in file:
            factoriel = calcul_factoriel(int(i))
            print (factoriel)
            res.append(str(factoriel))
    with open('doc.txt', 'w') as resultat:
        resultat.write("\n".join(res))

def update_history(msg):
    with open('history.txt', 'w') as his:
        his.write(msg)
             

if __name__== "__main__":
    try:
        update_history("Execution du programme suite au commit :" + commit)
        main()
    except Exception as e:
        # raise( Exception(f"Type incorrect, str({e})" ))
        with open('error.txt', 'w') as error_file:
            error_file.write(f"Type incorrect: {str(e)}")
    
